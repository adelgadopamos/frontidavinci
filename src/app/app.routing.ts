import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login.component';
import { RegisterComponent } from './components/register.component';
import { DefaultComponent } from './components/default.component';
import { UserEditComponent } from './components/user.edit.component';
import { SuperpersonaNewComponent} from './components/superpersonas.new.component';
import { SuperpersonaDetailComponent } from './components/superpersona.detail.component';
import { SuperpersonaEditComponent} from './components/superpersona.edit.component';

const appRoutes: Routes = [
	{path:'', component: DefaultComponent},
	{path:'index', component: DefaultComponent},
	{path:'index/:page', component: DefaultComponent},
	{path:'login',component: LoginComponent},
	{path:'login/:id',component: LoginComponent},
	{path:'register',component: RegisterComponent},
	{path:'user-edit',component: UserEditComponent},
	{path:'superpersonas-new',component: SuperpersonaNewComponent},
	{path:'superpersona/:id',component: SuperpersonaDetailComponent},
	{path:'superpersona-edit/:id',component: SuperpersonaEditComponent},
	{path:'**', component: LoginComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);