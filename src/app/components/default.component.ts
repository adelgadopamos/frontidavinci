import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';
import { SuperpersonaService } from '../services/superpersona.service';
import { Superpersona } from '../models/superpersona';

@Component({
	selector: 'default',
	templateUrl: '../views/default.html',
	providers: [UserService, SuperpersonaService]
})
export class DefaultComponent implements OnInit{
	public title: string;
	public identity;
	public token;
	public superpersonas;
	public pages;
	public pagePrev;
	public pageNext;
	public loading;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _superpersonaService: SuperpersonaService
	){
		this.title = 'HOME';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();


	}

	ngOnInit(){
		console.log('El componente default.component ha sido cargado!!');

		this.getAllSuperpersonas();

	}

	getAllSuperpersonas(){
		this._route.params.forEach((params: Params) => {
			let page = +params['page'];

			if(!page){
				page = 1;
			}

			this.loading = 'show';

			this._superpersonaService.getSuperpersonas(this.token, page).subscribe(
				response => {
					if(response.code == 200){
						this.superpersonas = response.data;
						console.log(this.superpersonas);
						this.loading = 'hidden';
						
						//Paginacion
						this.pages = [];
						for(let i = 0; i < response.total_pages; i++){
							this.pages.push(i);
						}

						//Prev Page
						if(page >= 2){
							this.pagePrev = (page -1);
						}else{
							this.pagePrev = page;
						}

						//Next page
						if(page < response.total_pages){
							this.pageNext = (page + 1);
						}else{
							this.pageNext = page;
						}

					}
				},
				error => {
					console.log(<any>error);
				}
			);
		});
	}

	deleteSuperpersona(id){
		console.log('Has pulsado en Borrar');

		this._superpersonaService.deleteSuperpersona(this.token, id).subscribe(
			response =>{ 
				if(response.status == 'success'){
					alert('Superpersona borrada correctamente');
					window.location.reload();
				}else{
					alert('No se ha borrado la superpersona');
				}
			},

			error =>{
				console.log(<any>error);
			}
		);
	}

}