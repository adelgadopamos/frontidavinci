import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {UserService} from '../services/user.service';
import {SuperpersonaService} from '../services/superpersona.service';
import {Superpersona} from '../models/superpersona';
import {User} from '../models/user';

@Component({
	selector: 'superpersona-detail',
	templateUrl: '../views/superpersona.detail.html',
	providers: [UserService, SuperpersonaService]
})

export class SuperpersonaDetailComponent implements OnInit{
	public identity;
	public token;
	public superpersona: Superpersona;
	public user: User;

	constructor(
		private _userService: UserService,
		private _superpersonaService: SuperpersonaService,
		private _route: ActivatedRoute,
		private _router: Router
	){
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();

	}

	ngOnInit(){
		if(this.identity && this.identity.sub){
			//Llamada al servicio de superpersonas para sacar una superpersona
			//LLamada a metodo de este componente
			this.getSuperpersona1();
		}else{
			this._router.navigate(['/login']);
		}
	}

	getSuperpersona1(){
		this._route.params.forEach((params: Params) => {
			let id = +params['id'];

			this._superpersonaService.getSuperpersona1(this.token, id).subscribe(
				response => {
					
					if(response.code == 200){
						this.superpersona = response.data;
						console.log(this.superpersona);
						
					}else{
						this._router.navigate(['/login']);
					}
				},
				error => {
					console.log(<any>error);
				}
			);
		});
	}
}