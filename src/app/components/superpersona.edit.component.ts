import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { UserService } from '../services/user.service';
import { SuperpersonaService } from '../services/superpersona.service';
import { Superpersona } from '../models/superpersona';

@Component({
	selector: 'superpersonas-edit',
	templateUrl: '../views/superpersona.new.html',
	providers: [UserService, SuperpersonaService]
})

export class SuperpersonaEditComponent implements OnInit{
	public title: string;
	public identity;
	public token;
	public superpersona: Superpersona;
	public status;

	constructor(
		private _userService: UserService,
		private _superpersonaService: SuperpersonaService,
		private _route: ActivatedRoute,
		private _router: Router
	){
		this.title = 'Editar Superpersona';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
	}

	ngOnInit(){
		if(this.identity == null && !this.identity.sub){
			this._router.navigate(['/login']);
		}else{
			//this.superpersona = new Superpersona(1,"","",0,0,0,"",null,null,null,null);
			this.getSuperpersona1();
		}

	}


	getSuperpersona1(){
		this._route.params.forEach((params: Params) => {
			let id = +params['id'];

			this._superpersonaService.getSuperpersona1(this.token, id).subscribe(
				response => {
					
					if(response.code == 200){
						this.superpersona = response.data;
						console.log(this.superpersona);
						
					}else{
						this._router.navigate(['/login']);
					}
				},
				error => {
					console.log(<any>error);
				}
			);
		});
	}


	onSubmit(){
		console.log(this.superpersona);
		this._route.params.forEach((params: Params) => {
			let id = +params['id'];
			this._superpersonaService.update(this.token, this.superpersona, id ).subscribe(
				response => {
					this.status = response.status;
					console.log(this.status);

					if(this.status != 'success'){
						this.status = 'error';
					}else{
						this.superpersona = response.data;

						this._router.navigate(['/']);
					}
				},
				error => {
					console.log(<any>error);
				}
			);
		});
	}
}