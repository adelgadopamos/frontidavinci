export class Superpersona{
	
	constructor(
		public id:number,
		public name:string,
		public residenceCity:string,
		public power:number,
		public intelligence:number,
		public health:number,
		public kind:string,
		public createdAt,
		public updatedAt,
		public createdBy,
		public modifiedBy
	){}
}