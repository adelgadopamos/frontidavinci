import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {GLOBAL} from './global';

@Injectable()
export class SuperpersonaService{
	public url:string;

	constructor(
		private _http: Http
	){
		this.url = GLOBAL.url;
	}

	create(token, superpersona){
		let json = JSON.stringify(superpersona);
		let params = "json="+json+"&authorization="+token;
		let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

		return this._http.post(this.url+'/superpersonas/new', params, {headers: headers}).pipe(map (res => res.json()));
	}

	getSuperpersonas(token, page = null){
		let params = "authorization="+token;
		let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

		if(page == null){
			page = 1;
		}

		return this._http.post(this.url+'/superpersonas/list?page='+page, params, {headers:headers}).pipe(map(res => res.json()));

	}


	getSuperpersona1(token, id){
		let params = "authorization="+token;
		let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

		return this._http.post(this.url+'/superpersonas/detail/'+id, params, {headers: headers}).pipe(map(res => res.json()));
	}

	update(token, superpersona, id){
		let json = JSON.stringify(superpersona);
		let params = "json="+json+"&authorization="+token;
		let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

		return this._http.post(this.url+'/superpersonas/edit/'+id, params, {headers: headers}).pipe(map(res => res.json()));
	}


	deleteSuperpersona(token, id){
		let params = "authorization="+token;
		let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

		return this._http.post(this.url+'/superpersonas/remove/'+id, params, {headers: headers}).pipe(map(res => res.json()));
	}
}